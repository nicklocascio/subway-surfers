// Nick Locascio

#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <vector>
#include <string>
#include <fstream>
using namespace std;
#include "gfx.h"
#include "character.h"

void drawSquare(int, int);
void drawBarrier(int, int);
void drawTrain(int, int);
void drawCoin(int, int);
void playGame();
vector<int> randomObstacles();
void fallingAnimation(int, int, vector<int>);
void displayInstructions();
void addToFile(int, string, string);

string name;
string filename = "scores.txt";
int score;
int colNum = 4;
int numObjects = 4;
const int wd = SIZE*colNum+SPACE*(colNum+1);
const int ht = SIZE*10;

struct Col
{
	bool isSquare = false;
	bool isBarrier = false;
	bool isTrain = false;
	bool isCoin = false;
};

vector<Col> cols(colNum);

// UP NEXT:
// allow user to pass in the number of columns they want to have
// add scoring system
// add hoverboard
// add slowed down time powerup
// maybe add a score multiplier?

int main(int argc, char *argv[])
{
	gfx_open(wd, ht, "My Game");

	if(argc == 1)
		name = "guest";
	else
		name = argv[1];

	cout << name << endl;

	//displayInstructions();
	
	char z;
	bool play = true;
	while(play)
	{
		score = 0;
		playGame();
		usleep(100000);
		gfx_text(wd/2-30, ht/2, "Score: ");
		//addToFile(score, name, filename);
		gfx_text(wd/2+10, ht/2, to_string(score).c_str());
		gfx_text(wd/2-50, ht/2+15, "To quit, press q");
		gfx_text(wd/2-100, ht/2+30, "To play again, press any key");
		z = gfx_wait();
		// this makes sure that the game won't start again before the user presses a key
		if(z == 'q')
			play = false;
		else
			z = gfx_wait();
	}

	return 0;	
}

void drawSquare(int x1, int y1)
{
	// draws a "filled in" square with "SIZE" different lines
	/*
	for(int i = 0; i < SIZE; i++)
	{
		gfx_line(x1, y1, x1+SIZE, y1);
		y1++;
	}
	*/
	gfx_line(x1, y1, x1+SIZE, y1);
	gfx_line(x1+SIZE, y1, x1+SIZE, y1+SIZE);
	gfx_line(x1+SIZE, y1+SIZE, x1, y1+SIZE);
	gfx_line(x1, y1+SIZE, x1, y1);

}

void drawBarrier(int x1, int y1)
{
	// essentially a square with only three sides
	gfx_line(x1, y1, x1+SIZE, y1);
	gfx_line(x1+SIZE, y1, x1+SIZE, y1+SIZE);
	gfx_line(x1, y1, x1, y1+SIZE);
}

void drawTrain(int x1, int y1)
{
	// same process as for a square, but twice the height
	/*
	for(int i = 0; i < SIZE*2; i++)
	{
		gfx_line(x1, y1-SIZE, x1+SIZE, y1-SIZE);
		y1++;
	}
	*/
	gfx_line(x1, y1-SIZE, x1+SIZE, y1-SIZE);
	gfx_line(x1+SIZE, y1-SIZE, x1+SIZE, y1+SIZE);
	gfx_line(x1+SIZE, y1+SIZE, x1, y1+SIZE);
	gfx_line(x1, y1+SIZE, x1, y1-SIZE);

}

void drawCoin(int x1, int y1)
{
	gfx_circle(x1+SPACE, y1+SPACE, 20);
	gfx_text(x1+SPACE, y1+SPACE, "$");
}

void playGame()
{
	bool playing = true;
	int x1 = SPACE, y1 = SPACE, y2 = SPACE;
	char c;
	Character character;
	bool jumping, ducking, boarding;
	int sleepTime = 4000;
	//vector<int> objects;
	//int col1, col2, col3;

	while(playing)
	{
		if(gfx_event_waiting())
			c = gfx_wait();

		if(c == 'q')
			playing = false;

		// generate random object for each column and clear previous object booleans
		for(int i = 0; i < cols.size(); i++)
		{
			cols[i].isSquare = false;
			cols[i].isBarrier = false;
			cols[i].isTrain = false;
			cols[i].isCoin = false;
		}
		vector<int> objects = randomObstacles();

		// animation of objects falling down the screen
		int count = 1, boardTime = 1;
		while(y1 < ht && playing)
		{	
			// display score in top right corner
			string score_s = to_string(score);
			char const *score_c = score_s.c_str();
			gfx_text(wd-65, 10, "Score: ");
			gfx_text(wd-25, 10, score_c);

			// this allows for the jump/duck animation to last for a certain time but doesn't get stuck there
			if(count > SIZE)
			{
				c = 'n';
				count = 0;
			}
			else
				c = 0;

			if(gfx_event_waiting())
				c = gfx_wait();
			
			// identify key presses
			switch(c)
			{
				// jump
				case 'w':
					jumping = true;
					ducking = false;
					break;
				// duck
				case 's':
					ducking = true;
					jumping = false;
					break;
				// move left
				case 'a':
					character.setCol(character.getCol()-1);
					character.setX(character.getX()-SPACE-SIZE);
					break;
				// move right
				case 'd':
					character.setCol(character.getCol()+1);
					character.setX(character.getX()+SPACE+SIZE);
					break;
				case 32:
					boarding = true;
					break;
				// set character to normal running position
				case 'n':
					jumping = false;
					ducking = false;
					break;
				case 'q':
					playing = false;
			}

			// ensures boarding only lasts for a certain amount of time
			if(boardTime > 1000)
				boarding = false;

			// display the character in the proper form
			if(jumping)
			{
				character.jumping();
				count++;
			}
			else if(ducking)
			{
				character.ducking();
				count++;
			}
			else if(boarding)
			{
				character.boarding();
			}
			else
				character.running();	

			// falling object animation with random objects
			fallingAnimation(x1, y1, objects);
			y1++;

			// this conditional checks if falling object is within range of the character
			if(y1+SIZE >= ht-personHt-SPACE && y1+SIZE < ht-personHt)
			{
				if(jumping)
				{
					// ensure character is jumping over a square
					if(cols[character.getCol()].isSquare)
					{
						// move obstacle down and increment score
						y1 = ht-personHt/2;
						score++;
					}
				}
				else if(ducking)
				{
					// ensure that character is ducking under a barrier
					if(cols[character.getCol()].isBarrier)
					{
						// move obstacle down and incremement score
						y1 = ht-personHt/2;
						score++;
					}
				}
			}

			if(boarding)
				boardTime++;

			// if the object hits character (unless it's a coin), game over
			else if(y1+SIZE == ht-personHt)
			{
				if(cols[character.getCol()].isCoin)
				{
					score += 2;
					y1 = ht-personHt/2;
				}
				else
				{
					gfx_clear();
					character.dead();
					usleep(1000000);
					playing = false;
				}
			}

			gfx_flush();
			usleep(sleepTime);
			gfx_clear();
		}

		// reset y position to top of screen
		y1 = SPACE;
		if(sleepTime > 2000)
			sleepTime -= 200;
		else if(sleepTime > 1500)
			sleepTime -= 5;

		//cout << sleepTime << endl;
	}
}

vector<int> randomObstacles()
{
	srand(time(NULL));
	// this vector will contain random obstacles in the indeces of the columns they will be placed in
	vector<int> obstacles;
	int randNum = 0;

	for(int i = 0; i < colNum-1; i++)
	{
		randNum = rand()%numObjects+1;
		obstacles.push_back(randNum);
	}

	randNum = rand()%2+1;
	obstacles.push_back(randNum);

	return obstacles;
}

void fallingAnimation(int x1, int y1, vector<int> objects)
{
	// this function is essentially designed to place the random object in each column
	// also determines which object is in which column with the Col struct
	for(int i = 0; i < objects.size(); i++)
	{
		//x1 = x1+SIZE*(i)+SPACE*(i+1);
		switch(objects[i])
		{
			case 1:
				cols[i].isSquare = true;
				drawSquare(x1+SIZE*i+SPACE*i, y1);
				break;
			case 2:
				cols[i].isBarrier = true;
				drawBarrier(x1+SIZE*i+SPACE*i, y1);
				break;
			case 3:
				cols[i].isTrain = true;
				drawTrain(x1+SIZE*i+SPACE*i, y1);
				break;
			case 4:
				cols[i].isCoin = true;
				drawCoin(x1+SIZE*i+SPACE*i, y1);
				break;
		}
	}
}

// very straightforward function, just lots of gfx_text() statements
void displayInstructions()
{
	char s = 0;

	while(s != 1)
	{
		if(gfx_event_waiting())
			s = gfx_wait();
			
		gfx_text(wd/2-SPACE-170, ht/2, "Welcome to the Knock-Off Subway Surfers! (click through directions)");
	}
	gfx_clear();
	s = 0;

	while(s != 1)
	{
		if(gfx_event_waiting())
			s = gfx_wait();

		gfx_text(wd/2-15-SPACE, ht/2, "Use w to jump");
		gfx_text(wd/2-15-SPACE, ht/2+15, "Use s to duck");
		gfx_text(wd/2-20-SPACE, ht/2+30, "Use a to move left");
		gfx_text(wd/2-20-SPACE, ht/2+45, "Use d to move right");
	}
	gfx_clear();
	s = 0;
	
	while(s != 1)
	{
		if(gfx_event_waiting())
			s = gfx_wait();

		gfx_text(wd/2-SPACE-25, ht/2, "You can jump over boxes:");
		drawSquare(wd/2-SPACE, ht/2+20);
	}
	gfx_clear();
	s = 0;

	while(s != 1)
	{
		if(gfx_event_waiting())
			s = gfx_wait();

		gfx_text(wd/2-SPACE-25, ht/2, "You can duck under hurdles:");
		drawBarrier(wd/2-SPACE, ht/2+20);
	}
	gfx_clear();
	s = 0;

	while(s != 1)
	{
		if(gfx_event_waiting())
			s = gfx_wait();

		gfx_text(wd/2-SPACE-25, ht/2, "You must avoid trains:");
		drawTrain(wd/2-SPACE, ht/2+SIZE+20);
	}
	gfx_clear();
	s = 0;

	while(s != 1)
	{
		if(gfx_event_waiting())
			s = gfx_wait();

		gfx_text(wd/2-SPACE-50, ht/2, "Picking up coins increases score by 2:");
		drawCoin(wd/2, ht/2);
	}
	gfx_clear();
	s = 0;

	while(s != 'p')
	{
		if(gfx_event_waiting())
			s = gfx_wait();

		gfx_text(wd/2-SPACE-25, ht/2, "Ready? Press p to play!");
	}
	gfx_clear();
	s = 0;
}

// this does not completely work yet, replaces the previous score each time
void addToFile(int score, string name, string filename)
{
	ofstream ofs;
	ofs.open(filename);

	string scoreLine = name + " " + to_string(score);
	ofs << scoreLine << endl;
}
