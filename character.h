// Nick Locascio
// character.h

const int SIZE = 80;
const int SPACE = 50;
//const int wd = SIZE*4+SPACE*5; //SIZE*3+SPACE*4; // 440
//const int ht = SIZE*10; // 800
const int personHt = (SIZE/2+SIZE/2+SIZE/4)*2;

class Character
{
	public:
		Character();
		void running();
		void jumping();
		void ducking();
		void boarding();
		void dead();
		void setX(int);
		int getX();
		void setCol(int);
		int getCol();
	private:
		int x;
		int y;
		int col;
};
