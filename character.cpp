// Nick Locascio
// implementation for character.h

#include <iostream>
#include <cmath>
using namespace std;
#include "gfx.h"
#include "character.h"

Character::Character() 
{ 
	x = SPACE+SPACE+SIZE+SIZE/2; // 220
	y = SIZE*8; //560
	col = 1;
}

void Character::setX(int a) { x = a; }
int Character::getX() { return x; };
void Character::setCol(int c) { col = c; }
int Character::getCol() { return col; }

void Character::running()
{
	gfx_circle(x, y, SIZE/4);
	// body
	gfx_line(x, y+SIZE/4, x, y+SIZE/2);
	// legs
	gfx_line(x, y+SIZE/2, x+SIZE/8, y+SIZE);
	gfx_line(x, y+SIZE/2, x-SIZE/8, y+SIZE);
	// arms
	gfx_line(x, y+SIZE/3, x-SIZE/4, y+SIZE/3);
	gfx_line(x, y+SIZE/3, x+SIZE/4, y+SIZE/3);

	gfx_flush();
}

void Character::jumping()
{
	gfx_circle(x, y, SIZE/4);
	// body
	gfx_line(x, y+SIZE/4, x, y+SIZE/2);
	// legs
	gfx_line(x, y+SIZE/2, x+SIZE/8, y+60);
	gfx_line(x, y+SIZE/2, x-SIZE/8, y+60);
	// arms
	gfx_line(x, y+SIZE/3, x-SIZE/4, y+20);
	gfx_line(x, y+SIZE/3, x+SIZE/4, y+20);

	gfx_flush();
}

void Character::ducking()
{
	gfx_circle(x, y, SIZE/6);
	// body
	gfx_line(x, y+SIZE/6, x, y+SIZE/3);
	// legs
	gfx_line(x, y+SIZE/3, x+SIZE/8, y+SIZE/1.5);
	gfx_line(x, y+SIZE/3, x-SIZE/8, y+SIZE/1.5);
	// arms
	gfx_line(x, y+SIZE/4, x-SIZE/4, y+40);
	gfx_line(x, y+SIZE/4, x+SIZE/4, y+40);

	gfx_flush();
}

void Character::boarding()
{
	gfx_circle(x, y, SIZE/4);
	// body
	gfx_line(x, y+SIZE/4, x, y+SIZE/2);
	// legs
	gfx_line(x, y+SIZE/2, x+SIZE/8, y+SIZE);
	gfx_line(x, y+SIZE/2, x-SIZE/8, y+SIZE);
	// arms
	gfx_line(x, y+SIZE/3, x-SIZE/4, y+SIZE/3);
	gfx_line(x, y+SIZE/3, x+SIZE/4, y+SIZE/3);
	// board
	gfx_line(x-SIZE/3, y+SIZE, x+SIZE/3, y+SIZE);
	gfx_line(x-SIZE/3, y+SIZE, x-SIZE/3-10, y+SIZE-10);
	gfx_line(x+SIZE/3, y+SIZE, x+SIZE/3+10, y+SIZE-10);

	gfx_flush();
}

void Character::dead()
{
	gfx_circle(x, y, SIZE/4);
	// body
	gfx_line(x, y+SIZE/3, x, y+SIZE/2);
	// legs
	gfx_line(x+10, y+SIZE/2, x+SIZE/4, y+SIZE);
	gfx_line(x-10, y+SIZE/2, x-SIZE/4, y+SIZE);
	// arms
	gfx_line(x-SIZE, y+SIZE/3, x-SIZE/2, y+SIZE/3);
	gfx_line(x+SIZE, y+SIZE/3, x+SIZE/2, y+SIZE/3);

	gfx_flush();
}
