CMP = g++
CLASS1 = character
MAIN = project
EXEC = project

$(EXEC): $(CLASS1).o $(MAIN).o gfx.o
	$(CMP) $(CLASS1).o $(MAIN).o gfx.o -lX11 -o $(EXEC)

$(CLASS1).o: $(CLASS1).cpp $(CLASS1).h
	$(CMP) -std=c++11 -c $(CLASS1).cpp -lX11 -o $(CLASS1).o

$(MAIN).o: $(MAIN).cpp $(CLASS1).h 
	$(CMP) -std=c++11 -c $(MAIN).cpp -lX11 -o $(MAIN).o

clean:
	rm $(MAIN).o
	rm $(CLASS1).o
	rm $(EXEC)
